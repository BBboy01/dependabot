# Local configuration

If all of your projects have the same structure or you simply want to define default configuration to fall back to if repository doesn't have a configuration file, you can define a local configuration file location.

This option is defined by `SETTINGS__CONFIG_LOCAL_FILENAME` environment variable. Project specific configuration will always take precedence over local configuration.

See: [common-configuration](./environment.md#configuration-file) section for more details.
