# Deploy modes

There are several ways to deploy `dependabot-gitlab` app.

## Helm chart

The preferred deployment type is deploying on kubernetes cluster using official [helm chart](https://gitlab.com/dependabot-gitlab/chart).

Add helm repo:

```sh
helm repo add dependabot https://dependabot-gitlab.gitlab.io/chart
```

Install application:

```sh
helm install dependabot dependabot/dependabot-gitlab --values values.yaml
```

For the full list of configurable options, see [values](https://gitlab.com/dependabot-gitlab/chart#values).

### Terraform

[deploy](https://gitlab.com/dependabot-gitlab/dependabot/-/blob/main/deploy) directory contains [terraform](https://www.terraform.io/) configuration example to deploy `dependabot-gitlab` app on [GKE](https://cloud.google.com/kubernetes-engine) cluster.

## Docker compose

It is possible to use [docker-compose.yml](https://gitlab.com/dependabot-gitlab/dependabot/-/blob/main/docker-compose.yml) to start the app using [docker compose](https://docs.docker.com/compose/) `up` command:

```sh
docker compose up -d
```

::: info
Application internally uses [docker-in-docker](https://www.howtogeek.com/devops/how-and-why-to-run-docker-inside-docker/) setup to start `updater` containers. When debugging issues with failures to download docker images, take note that nothing is shared with the host machine and all docker images are downloaded inside `worker` container.
:::

### Custom docker-compose.yml

By default, `core` docker image will contain a copy of [docker-compose.yml](https://gitlab.com/dependabot-gitlab/dependabot/-/blob/main/docker-compose.yml) file which is used by app to dynamically start `updater` containers.

In case a custom `docker-compose.yml` file is used (to provide additional environment variables for example), it needs to be also mounted inside `worker` container, so that application can correctly pass additional environment variables to `updater` containers.

This is done by mounting the custom `docker-compose.yml` file via `volumes` instruction and defining path inside container via `SETTINGS__UPDATER_TEMPLATE_PATH` environment variable.

Example:

```yaml
x-environment: &environment_variables
  SOME_ADDITIONAL_CUSTOM_ENV_VAR: foo

...

services:
  worker:
    image: *base_image
    depends_on:
      - redis
      - migration
    volumes:
      - tmp:/home/dependabot/app/tmp
      - docker-certs-client:/certs/client:ro
      - $PWD/custom-docker-compose.yml:/home/dependabot/app/docker-compose.yml
    environment:
      <<: *environment_variables
      SETTINGS__UPDATER_TEMPLATE_PATH: /home/dependabot/app/docker-compose.yml
```
