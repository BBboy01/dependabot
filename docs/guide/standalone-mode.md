# Standalone mode

Application can be used in a stateless cli like mode. This mode is most useful to run dependency updates from [Gitlab CI](https://docs.gitlab.com/ee/ci/).

[dependabot-gitlab/dependabot-standalone](https://gitlab.com/dependabot-gitlab/dependabot-standalone) describes the best way to run dependency updates from gitlab ci as simple stateless jobs.

For custom configuration options see [environment variables](../config/environment.md).

Simple stateless setup does not support many of the more advanced features like:

- merge request commands
- webhook integration with GitLab instance
- management via UI
