ARG BASE_IMAGE=ghcr.io/dependabot-gitlab/dependabot-updater-core:latest
ARG DOCKER_VERSION=25.0.3

FROM docker:${DOCKER_VERSION}-cli as docker
FROM ${BASE_IMAGE} as core

FROM core as development

USER root

ARG NODE_MAJOR=20
ARG HOME=/home/dependabot
ARG CODE_DIR=${HOME}/app
ENV DEPENDABOT_NATIVE_HELPERS_PATH=${HOME}/.cache/helpers \
    APP_START_SCRIPT=${HOME}/start_app.sh

# Install Node.js
RUN set -eux; \
    apt-get update; \
    mkdir -p /etc/apt/keyrings; \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

RUN set -eux; \
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list; \
    apt-get update && apt-get install nodejs -y

# Install browser system deps
RUN set -eux; \
    apt-get install -y --no-install-recommends libglib2.0-0 \
    libnss3 \
    libnspr4 \
    libdbus-1-3 \
    libatk1.0-0 \
    libatk-bridge2.0-0 \
    libcups2 \
    libdrm2 \
    libxcb1 \
    libxkbcommon0 \
    libatspi2.0-0 \
    libx11-6 \
    libxcomposite1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxrandr2 \
    libgbm1 \
    libpango-1.0-0 \
    libcairo2 \
    libasound2

USER dependabot

WORKDIR ${CODE_DIR}

# Add app startup script
RUN set -eux; \
    mkdir -p ${HOME}/log && \
    echo "#!/bin/bash" > ${APP_START_SCRIPT} && \
    echo "set -e" >> ${APP_START_SCRIPT} && \
    echo "rm -rf ${CODE_DIR}/tmp/pids/server.pid" >> ${APP_START_SCRIPT} && \
    echo "bundle exec sidekiq > ${CODE_DIR}/log/sidekiq.log 2>&1 &" >> ${APP_START_SCRIPT} && \
    echo "bundle exec rails server > ${CODE_DIR}/log/puma.log 2>&1 &" >> ${APP_START_SCRIPT} && \
    echo "sleep 5 && echo \"Started dependabot-giltab! Logging to '${CODE_DIR}/log' directory.\"" >> ${APP_START_SCRIPT} && \
    echo "[ \"\${1}\" == \"tail\" ] && tail -f ${CODE_DIR}/log/puma.log || exit" >> ${APP_START_SCRIPT} && \
    chmod +x ${APP_START_SCRIPT} && \
    echo "alias start_app='bash ${APP_START_SCRIPT}'" >> ${HOME}/.bash_aliases

# Create folders for mounting
RUN mkdir -p ${HOME}/.cache/ms-playwright && \
    mkdir -p ${HOME}/.cache/npm && \
    mkdir -p ${CODE_DIR}/tmp && \
    mkdir -p ${CODE_DIR}/node_modules

# Install gems
COPY --chown=dependabot:dependabot Gemfile Gemfile.lock ${CODE_DIR}/
RUN bundle install

FROM core as production

USER root

# Install system deps
RUN set -eux; \
    apt-get update && apt-get install -y --no-install-recommends \
    autoconf \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/*;

# Install jemmaloc
ARG JEMALLOC_VERSION=5.3.0
ARG JEMALLOC_DOWNLOAD_URL="https://github.com/jemalloc/jemalloc/releases/download/${JEMALLOC_VERSION}/jemalloc-${JEMALLOC_VERSION}.tar.bz2"
RUN set -eux; \
    mkdir -p /usr/src/jemalloc \
    && cd /usr/src/jemalloc \
    && curl --fail --location --output jemalloc.tar.bz2 ${JEMALLOC_DOWNLOAD_URL}; \
    tar -xjf jemalloc.tar.bz2 && cd jemalloc-${JEMALLOC_VERSION}; \
    ./autogen.sh --prefix=/usr \
    && make -j "$(nproc)" install \
    && make clean; \
    rm -rf ../jemalloc.tar.bz2

# Install docker and compose plugin
ARG DOCKER_VERSION
RUN set -eux; \
    mkdir -m 0755 -p /etc/apt/keyrings \
    && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg \
    && echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null; \
    apt-get update && apt-get install -y --no-install-recommends \
    docker-ce-cli=5:${DOCKER_VERSION}* \
    && mkdir -p /usr/libexec/docker/cli-plugins \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/*;
COPY --from=docker /usr/local/libexec/docker/cli-plugins/docker-compose /usr/libexec/docker/cli-plugins/docker-compose

USER dependabot
WORKDIR /home/dependabot/app

ENV BUNDLE_PATH=vendor/bundle \
    BUNDLE_WITHOUT="development:test:assets" \
    LD_PRELOAD=/usr/lib/libjemalloc.so \
    RAILS_ENV=production

# Copy gemfile first so cache can be reused
COPY --chown=dependabot:dependabot Gemfile Gemfile.lock ./
RUN set -eux; \
    bundle install \
    && bundle exec bootsnap precompile --gemfile \
    && gem clean \
    && bundle clean \
    && rm -rf vendor/bundle/ruby/*/cache

COPY --chown=dependabot:dependabot ./ ./

# Precompile bootsnap code for faster boot times
RUN set -eux; \
    bundle exec bootsnap precompile app/ lib/; \
    SECRET_KEY_BASE_DUMMY=1 SETTINGS__GITLAB_ACCESS_TOKEN=token bundle exec rails about

ARG COMMIT_SHA
ARG PROJECT_URL
ARG VERSION
ARG BASE_IMAGE

ENV APP_VERSION=$VERSION

LABEL org.opencontainers.image.authors="andrejs.cunskis@gmail.com" \
    org.opencontainers.image.source=$PROJECT_URL \
    org.opencontainers.image.revision=$COMMIT_SHA \
    org.opencontainers.image.version=$VERSION \
    org.opencontainers.image.base.name=$BASE_IMAGE

ENTRYPOINT [ "bundle", "exec" ]
