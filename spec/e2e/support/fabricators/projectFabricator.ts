import { APIRequestContext, APIResponse, expect } from "@playwright/test";
import { Smocker } from "../mocks/smocker";
import { mocks } from "../mocks/mocks";

export class ProjectFabricator {
  private _initialized: boolean;
  private _headers: { [key: string]: string };
  private _smocker: Smocker;
  private _request: APIRequestContext;

  constructor(request: APIRequestContext, user) {
    this._initialized = false;
    this._request = request;
    this._smocker = new Smocker();
    this._headers = {
      "Content-Type": "application/json",
      ...user.basicAuthHeader
    };
  }

  public get smocker() {
    this.init();

    return this._smocker;
  }

  async create(projectName: string, options: { token?: string; reset?: boolean } = { reset: true }) {
    await this.init();

    if (options.reset) await this.smocker.reset();
    await this.smocker.add(mocks.registerProject(projectName));

    const response = await this._request.post("/api/v2/projects", {
      headers: this._headers,
      data: { project_name: projectName, gitlab_access_token: options?.token }
    });
    const responseJson = await response.json();
    this.errorIfNotOk(response);

    return responseJson;
  }

  private errorIfNotOk(response: APIResponse) {
    expect(response.ok(), `Expected response to be ok, but was ${response.status()}`).toBeTruthy();
  }

  private async init() {
    if (this._initialized) return;

    await this._smocker.init();
    this._initialized = true;
  }
}
