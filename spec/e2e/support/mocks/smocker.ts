import { APIRequestContext, request, expect, APIResponse } from "@playwright/test";
import { randomInt } from "crypto";
import { readFileSync } from "fs";
import Handlebars from "handlebars";

export class Smocker {
  readonly baseUrl: string;
  readonly headers: { headers: { [key: string]: string } };

  context: APIRequestContext;

  constructor() {
    this.baseUrl = `http://${process.env.MOCK_HOST || "localhost"}:8081`;
    this.headers = { headers: { "Content-Type": "application/x-yaml" } };
  }

  async init() {
    this.context = await request.newContext({ baseURL: this.baseUrl });

    return this;
  }

  async reset() {
    const response = await this.context.post("/reset");
    this.checkResponse(response);
  }

  async add(params: { projectName: string; definitions: Array<{ name: string; type?: string; params?: {} }> }) {
    const yaml = this.definitionYml(params.projectName, params.definitions);

    const response = await this.context.post("/mocks", {
      ...this.headers,
      data: yaml
    });
    this.checkResponse(response);
  }

  async verify() {
    const response = await this.context.post("/sessions/verify");
    this.checkResponse(response);

    const responseJson = await response.json();
    const { verified, all_used, failures, unused } = responseJson.mocks;
    const message = ["Mock verify failed"];
    if (failures) message.push(`- failures: ${JSON.stringify(failures, null, 2)}`);
    if (unused) message.push(`- unused: ${JSON.stringify(unused, null, 2)}`);

    expect({ verified, all_used }, message.join("\n")).toEqual({ verified: true, all_used: true });
  }

  async dispose() {
    await this.context.dispose();
  }

  private async checkResponse(response: APIResponse) {
    expect(
      response.ok(),
      `Expected response to be ok, was: ${response.status()}, ${response.statusText()}`
    ).toBeTruthy();
  }

  private defaultMockParams(projectName: string) {
    return {
      project_name: projectName,
      id: randomInt(1000000),
      iid: randomInt(1000000)
    };
  }

  private definitionYml(projectName: string, definitions: Array<{ name: string; type?: string; params?: {} }>) {
    const definitionYmls = definitions.map((definition) => {
      const fileName = `spec/fixture/${definition.type || "gitlab"}/mocks/dynamic/${definition.name}.yml`;
      const template = Handlebars.compile(readFileSync(fileName).toString(), { noEscape: true });

      return template({ ...this.defaultMockParams(projectName), ...definition.params });
    });

    return definitionYmls.join("\n");
  }
}
