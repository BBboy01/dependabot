import { test as setup } from '@playwright/test';
import { teardownE2eEnvironment } from "@support/env-setup";

setup("stop environment", async () => {
  await teardownE2eEnvironment();
});
