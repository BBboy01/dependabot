# frozen_string_literal: true

FactoryBot.define do
  factory :update_run, class: "Update::Run" do
    job { create(:update_job) }
    status { Update::Run::SUCCESS }

    factory :update_run_with_failures do
      status { Update::Run::FAILED }

      after(:create) do |update_run|
        create(:update_failure, run: update_run)
        create(:update_log_entry, run: update_run)
      end
    end
  end
end
