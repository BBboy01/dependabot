# frozen_string_literal: true

describe Job::Routers::DependencyUpdate do
  let(:project_name) { "project_name" }
  let(:package_ecosystem) { "package_ecosystem" }
  let(:directory) { "directory" }

  before do
    allow(Container::Compose::Runner).to receive(:call)
  end

  around do |example|
    with_env("SETTINGS__DEPLOY_MODE" => "compose") { example.run }
  end

  it "calls container runner class" do
    described_class.call(project_name, package_ecosystem, directory)

    expect(Container::Compose::Runner).to have_received(:call).with(
      package_ecosystem: package_ecosystem,
      task_name: "update",
      task_args: [project_name, package_ecosystem, directory]
    )
  end
end
