# Reusable rules
.mr: &mr
  if: '$CI_MERGE_REQUEST_IID'
.web: &web
  if: '$CI_PIPELINE_SOURCE == "web"'
.merge_train: &merge_train
  if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"'
.main: &main
  if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"'
.release: &release
  if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(-alpha\.\d+)?$/ && $CI_PIPELINE_SOURCE == "push"'
.fork: &fork
  if: '$CI_PROJECT_ROOT_NAMESPACE != "dependabot-gitlab"'
.mr-build-all-images: &mr-build-all-images
  if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_LABELS =~ /pipeline:build-all-images/'
.mr-run-all-tests: &mr-run-all-tests
  if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_LABELS =~ /pipeline:run-all-tests/'
.main_parent_pipeline: &main_parent_pipeline
  if: '$CI_PIPELINE_SOURCE == "parent_pipeline" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
.manual_deploy: &manual_deploy
  if: '$DEPLOY == "true"'
.version_update: &version_update
  if: '$CI_PIPELINE_SOURCE == "web" && $VERSION_COMPONENT != "none"'

.deploy_changes: &deploy_changes
  - app/**/*
  - config/**/*
  - db/**/*
  - deploy/*.tf
  - deploy/.terraform.lock.hcl
  - kube/**/*
  - lib/tasks/dependabot.rake
  - public/**/*
  - config.ru
  - Dockerfile.*
  - Gemfile
  - Gemfile.lock
  - Rakefile

.dependency_changes: &dependency_changes
  - Gemfile.lock
  - package-lock.json
  - Dockerfile.*

.db_changes: &db_changes
  - db/**/*.rb

.doc_changes: &doc_changes
  - docs/**/*
  - package-lock.json
  - app/api/v2/**/*.rb

.tf_changes: &tf_changes
  - deploy/*.tf
  - deploy/.terraform.lock.hcl

.e2e_test_changes: &e2e_test_changes
  - app/javascript/**/*
  - app/assets/**/*
  - app/views/**/*
  - app/api/v2/**/*.rb
  - app/controllers/**/*.rb
  - app/helpers/**/*.rb
  - app/models/**/*.rb
  - app/services/**/*.rb
  - spec/e2e/**/*
  - playright.config.ts
  - docker-compose.yml

# ======================================================================================================================
# Rules
# ======================================================================================================================

# ---------------------------------------------------------------------------------------------
# Pre stage and common rules
# ---------------------------------------------------------------------------------------------
.rules:dont-interrupt:
  rules:
    - *main
    - *release
    - *web
    - *main_parent_pipeline

.rules:cache:
  rules:
    - *main # run on main to regenerate if cache was cleared
    - <<: *mr
      changes: *dependency_changes

.rules:main:
  rules:
    - *main
    - *release
    - *mr

# ---------------------------------------------------------------------------------------------
# Build stage rules
# ---------------------------------------------------------------------------------------------
.rules:build-docs:
  rules:
    - <<: *mr
      changes: *doc_changes
      variables:
        DOCS_URL_BASE: /-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/vitepress
    - <<: *main
      changes: *doc_changes
      variables:
        DOCS_URL_BASE: /-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/vitepress
    - *release

.rules:compile-assets:
  rules:
    - <<: *version_update
      when: never
    - *fork
    - *main
    - *release
    - *web
    - *mr-run-all-tests
    - *mr-build-all-images
    - <<: *mr
      changes: *e2e_test_changes
    - <<: *mr
      changes: *dependency_changes

.rules:build-core-image:
  rules:
    - <<: *version_update
      when: never
    - <<: *fork
    - <<: *main
      variables:
        LATEST_TAG: main-latest
        BUILD_PLATFORM: linux/amd64,linux/arm64
    - <<: *release
      variables:
        BUILD_PLATFORM: linux/amd64,linux/arm64
    - <<: *mr-build-all-images
      variables:
        BUILD_PLATFORM: linux/amd64,linux/arm64
    - <<: *mr
      changes: *e2e_test_changes
    - <<: *mr
      changes: *dependency_changes
    - *mr-run-all-tests
    - *web

.rules:build-ecosystem-images:
  rules:
    - <<: *version_update
      when: never
    - <<: *fork
    - <<: *main
      variables:
        LATEST_TAG: main-latest
        BUILD_PLATFORM: linux/amd64,linux/arm64
    - <<: *release
      variables:
        BUILD_PLATFORM: linux/amd64,linux/arm64
    - <<: *mr-build-all-images
      variables:
        BUILD_PLATFORM: linux/amd64,linux/arm64
    - <<: *mr
      changes: *dependency_changes
    - *web

# ---------------------------------------------------------------------------------------------
# Static analysis stage rules
# ---------------------------------------------------------------------------------------------
.rules:dependency-scan:
  rules:
    - <<: *main
      changes: *dependency_changes
    - <<: *merge_train
      changes: *dependency_changes

# ---------------------------------------------------------------------------------------------
# Test stage rules
# ---------------------------------------------------------------------------------------------
.rules:migration-test:
  rules:
    - <<: *fork
    - <<: *main
      changes: *db_changes
    - <<: *mr
      changes: *db_changes
    - <<: *merge_train
      changes: *db_changes
    - *mr-run-all-tests
    - *release

.rules:e2e-test:
  rules:
    - <<: *fork
    - *main
    - *release
    - *mr-run-all-tests
    - <<: *mr
      changes: *e2e_test_changes
    - <<: *mr
      changes: *dependency_changes

.rules:standalone-test:
  rules:
    - *main
    - *release
    - *mr-run-all-tests
    - <<: *mr
      changes: *e2e_test_changes
    - <<: *mr
      changes: *dependency_changes

# ---------------------------------------------------------------------------------------------
# Report stage rules
# ---------------------------------------------------------------------------------------------
.rules:allure-reports:
  rules:
    - <<: *fork
      when: never
    - *main
    - *mr

.rules:coverage:
  rules:
    - <<: *fork
      when: never
    - *main
    - *mr

.rules:e2e-test-report:
  rules:
    - !reference [.rules:e2e-test, rules]

# ---------------------------------------------------------------------------------------------
# Release stage rules
# ---------------------------------------------------------------------------------------------
.rules:release:
  rules:
    - <<: *fork
      when: never
    - *release

.rules:release-docs:
  rules:
    - <<: *fork
      when: never
    - *release

.rules:bump-version:
  rules:
    - <<: *fork
      when: never
    - *version_update

# ----------------------------------------------------------------------------------------------------------------------
# Deploy stage rules
# ----------------------------------------------------------------------------------------------------------------------
.rules:deploy:
  rules:
    - <<: *version_update
      when: never
    - <<: *fork
      when: never
    - <<: *mr
      changes: *tf_changes
    - <<: *main
      changes: *deploy_changes
    - <<: *mr-build-all-images
      allow_failure: true
    - <<: *manual_deploy

.rules:tf:init:
  rules:
    - when: always

.rules:tf:build:
  rules:
    - <<: *mr
      changes: *tf_changes

.rules:tf:static-analysis:
  rules:
    - <<: *manual_deploy
      when: never
    - <<: *mr
      changes: *tf_changes
    - <<: *main_parent_pipeline
      changes: *tf_changes

.rules:tf:deploy:
  rules:
    - *main_parent_pipeline
    - <<: *mr-build-all-images
      allow_failure: true
      when: manual # allows to override variables on manual deploy
