# frozen_string_literal: true

module V1
  class API < Grape::API
    prefix "api"

    mount V1::Projects
    mount V1::NotifyRelease
    mount V1::Hooks
  end
end
