# frozen_string_literal: true

module Dependabot
  module Options
    # Dependency group related options
    #
    class DependencyGroups < OptionsBase
      # Transoform dependency update groups options
      #
      # @return [Hash]
      def transform
        groups = opts[:groups]
        return {} unless groups

        { groups: groups.deep_transform_keys(&:to_s) }
      end
    end
  end
end
