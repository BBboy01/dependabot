# frozen_string_literal: true

module Dependabot
  module Options
    class OptionsBase
      def initialize(opts)
        @opts = opts
      end

      private

      attr_reader :opts
    end
  end
end
