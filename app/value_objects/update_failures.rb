# frozen_string_literal: true

# Thread local global error store
#
class UpdateFailures
  class << self
    # Capture update job error
    #
    # @param [Error] error
    # @return [void]
    def add(error)
      fetch.push({
        klass: error.class,
        message: error.message,
        backtrace: error.backtrace
      })
    end

    # General update failures
    #
    # @return [Array]
    def update_failures
      fetch.reject { |error| error[:klass] == Dependabot::Update::Checker::UpdateImpossible }
    end

    # Failures with update impossible errors
    #
    # @return [Array]
    def update_impossible_failures
      fetch.select { |error| error[:klass] == Dependabot::Update::Checker::UpdateImpossible }
    end

    private

    # Current failures
    #
    # @return [Array]
    def fetch
      RequestStore.fetch(:errors) { [] }
    end
  end
end
