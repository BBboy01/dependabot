# frozen_string_literal: true

module Job
  module Routers
    module MergeRequest
      # :reek:InstanceVariableAssumption

      # Merge request update router
      #
      # Perform merge request rebase or fall back to recreation when conflicts are present
      #
      class Update < Base
        using Rainbow

        SKIP_DETAILS = {
          closed: { level: :warn, reason: "Merge request is not in opened state!" },
          assignee_not_matched: { level: :warn, reason: "Merge request assignee doesn't match configured one!" },
          strategy: { level: :info, reason: "Rebase strategy is 'auto' and no conflicts present" }
        }.freeze

        def initialize(project_name:, mr_iid:, ecosystem_update: false)
          super(project_name: project_name, mr_iid: mr_iid)
          @ecosystem_update = ecosystem_update
        end

        def call
          log(:info, "Running update for merge request #{web_url.bright}".strip)
          if skip_details
            return log(skip_details[:level], "  skipped merge request #{loggable_mr_iid}. #{skip_details[:reason]}")
          end

          conflicts? ? recreate_mr : rebase_mr
        end

        private

        attr_reader :ecosystem_update

        delegate :configuration, to: :project, prefix: true

        # Config entry for specific package ecosystem and directory
        #
        # @return [Hash]
        def config_entry
          @config_entry ||= project_configuration
                            .entry(package_ecosystem: package_ecosystem, directory: directory)
                            .tap { |entry| raise_missing_entry_error unless entry }
        end

        # Auto-rebase assignee option
        #
        # @return [Boolean]
        def rebase_assignee
          @rebase_assignee ||= config_entry.dig(:rebase_strategy, :with_assignee)
        end

        # Merge request assignee
        #
        # @return [String]
        def mr_assignee
          @mr_assignee ||= gitlab_mr.to_h.dig("assignee", "username")
        end

        # Gitlab merge request
        #
        # @return [Gitlab::ObjectifiedHash]
        def gitlab_mr
          @gitlab_mr ||= gitlab.merge_request(project_name, mr_iid)
        end

        # Skipping details based on conditions
        #
        # @return [Hash]
        def skip_details # rubocop:disable Metrics/CyclomaticComplexity
          return @details if @details
          return @details = SKIP_DETAILS[:closed] if gitlab_mr.state != "opened"
          return @details = SKIP_DETAILS[:assignee_not_matched] if rebase_assignee && rebase_assignee != mr_assignee

          @details = SKIP_DETAILS[:strategy] if ecosystem_update && (rebase_auto? && !conflicts?)
        end

        # Emphasized loggable mr iid
        #
        # @return [String]
        def loggable_mr_iid
          "!#{mr_iid}".bright
        end

        # Is mr updateable
        # * mr in open state
        # * mr has correct assignee when configured and auto-rebase action performed
        #
        # @return [Boolean]
        def updateable?
          gitlab_mr.state == "opened" && (!rebase_assignee || rebase_assignee == mr_assignee)
        end

        def conflicts?
          gitlab_mr["has_conflicts"]
        end

        # Rebase strategy is set to auto
        #
        # @return [Boolean]
        def rebase_auto?
          config_entry.dig(:rebase_strategy, :strategy) == Dependabot::Options::Rebase::AUTO
        end

        # Rebase merge request
        #
        # @return [void]
        def rebase_mr
          gitlab.rebase_merge_request(project_name, mr_iid)

          log(:info, "  rebased merge request #{loggable_mr_iid}.")
        end

        # Recreate merge request
        #
        # @return [void]
        def recreate_mr
          container_runner_class.call(
            package_ecosystem: package_ecosystem,
            task_name: "recreate_mr",
            task_args: [project_name, mr_iid]
          )
        end
      end
    end
  end
end
