# frozen_string_literal: true

module Job
  module Routers
    class DependencyUpdate < ApplicationService
      include ServiceHelpersConcern

      def initialize(project_name, package_ecosystem, directory)
        @project_name = project_name
        @package_ecosystem = package_ecosystem
        @directory = directory
      end

      def call
        # Allow to bypass container runner in development mode
        return run_via_sidekiq if !Rails.env.production? && UpdaterConfig.deploy_mode.nil?

        container_runner_class.call(
          package_ecosystem: package_ecosystem,
          task_name: "update",
          task_args: [project_name, package_ecosystem, directory]
        )
      end

      private

      attr_reader :project_name, :package_ecosystem, :directory

      # Trigger update run directly via sidekiq
      #
      # @return [void]
      def run_via_sidekiq
        Job::Triggers::DependencyUpdate.call(project_name, package_ecosystem, directory)
      end
    end
  end
end
