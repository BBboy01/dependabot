# frozen_string_literal: true

module Job
  module Triggers
    module ServiceMode
      module DependencyUpdate
        def call
          update_run.update_attributes!(status: Update::Run::RUNNING)

          super
        ensure
          save_execution_details
        end

        private

        # Project
        #
        # @return [Project]
        def project
          @project ||= Project.find_by(name: project_name)
        end

        # Update job
        #
        # @return [Update::Job]
        def update_job
          @update_job ||= project.update_jobs.find_or_initialize_by(
            package_ecosystem: package_ecosystem,
            directory: directory
          )
        end

        # Dependency update run
        #
        # @return [Update::Run]
        def update_run
          @update_run ||= Update::Run.create!(job: update_job)
        end

        # Persist execution details
        #
        # @return [void]
        def save_execution_details
          finished_at = Time.zone.now
          failures = UpdateFailures.update_failures
          impossible_updates = UpdateFailures.update_impossible_failures
          status = if !failures.empty?
                     Update::Run::FAILED
                   elsif !impossible_updates.empty?
                     Update::Run::WARNING
                   else
                     Update::Run::SUCCESS
                   end

          update_run_details(finished_at, status, failures)
          update_project_details(finished_at, status)
        end

        # Update run details
        #
        # @param [Time] finished_at
        # @param [String] status
        # @param [Array] errors
        # @return [void]
        def update_run_details(finished_at, status, errors)
          update_run.update_attributes!(finished_at: finished_at, status: status)
          update_run.save_errors!(errors)
          update_run.save_log_entries!(UpdateLogs.fetch)
        end

        def update_project_details(finished_at, status)
          project.update_attributes!(last_update_run_at: finished_at)
          project.update_attributes!(last_run_status: status)
        end
      end
    end
  end
end
