# frozen_string_literal: true

module ConfigHelper
  # Vulnerability alerts enabled?
  #
  # @param [Hash] config_entry
  # @return [Boolean]
  def vulnerability_alerts?(config_entry)
    config_entry.dig(:vulnerability_alerts, :enabled)
  end
end
