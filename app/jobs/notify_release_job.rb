# frozen_string_literal: true

class NotifyReleaseJob < ApplicationJob
  queue_as :low

  sidekiq_options retry: false

  # Trigger package updates
  #
  # @param [String] name
  # @param [String] package_ecosystem
  # @param [String] project_name
  # @return [void]
  def perform(dependency_name, package_ecosystem, project_name)
    Job::Routers::NotifyRelease.call(dependency_name, package_ecosystem, project_name)
  end
end
