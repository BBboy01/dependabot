import { Controller } from "@hotwired/stimulus";

// Connects to data-controller="projects"
export default class extends Controller {
  static targets = ["tabPane"];
  static values = { url: String };

  errorHtml = `
    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert" id="projects-fetch-alert">
      <strong>Oops, something went wrong!</strong>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  `;

  connect() {
    document.getElementById("filter-form").reset();

    this.renderProjectsTable(`${this.urlValue}?active=true`, "active");
  }

  selectTab({ target }) {
    const id = target.getAttribute("data-bs-target").replace("#", "");
    const url = `${this.urlValue}?active=${id === "active"}`;
    this.renderProjectsTable(url, id);
  }

  selectPage(event) {
    event.preventDefault();

    const url = event.target.getAttribute("href");
    this.renderProjectsTable(url, this.currentlyActiveTabPane().id);
  }

  filter(event) {
    event.preventDefault();

    const active = this.currentlyActiveTabPane().id === "active";
    const form = event.target;
    const formData = new FormData(form);
    const params = new URLSearchParams([...formData.entries(), ["active", active]]).toString();
    const uri = form.getAttribute("action");
    this.renderProjectsTable(`${uri}?${params}`, this.currentlyActiveTabPane().id);
  }

  renderProjectsTable(url, id) {
    fetch(url, {
      method: "GET",
      headers: {
        "X-CSRF-Token": document.querySelector("meta[name=csrf-token]")?.content,
        "Content-Type": "application/json"
      }
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Oops, something went wrong!");
        }

        return response.text();
      })
      .then((html) => (this.tabPane(id).innerHTML = html))
      .catch(() => {
        this.tabPane(id).innerHTML = this.errorHtml;

        const alert = document.getElementById("projects-fetch-alert");
        alert.addEventListener("closed.bs.alert", () => location.reload());
      });
  }

  tabPane(id) {
    return this.tabPaneTargets.find((tabPane) => tabPane.id === id);
  }

  currentlyActiveTabPane() {
    return this.tabPaneTargets.find((tabPane) => tabPane.classList.contains("active"));
  }
}
