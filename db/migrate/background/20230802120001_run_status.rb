# frozen_string_literal: true

class RunStatus < Mongoid::Migration
  # :reek:ManualDispatch
  def self.up
    Update::Run.batch_size(1000).each do |run|
      run.update_attributes!(failed: run.failures.any?) if run.respond_to?(:failed)
    end
  end

  def self.down; end
end
